//
//  StatusView.m
//  Countly Menubar
//
//  Created by Paul Brown on 2014/11/24.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import "StatusView.h"

#define ImageViewWidth 22


@interface StatusView()
{
    
    

}
@end



@implementation StatusView

- (id)init
{
    CGFloat height = [NSStatusBar systemStatusBar].thickness;
    
    
    self = [super initWithFrame:NSMakeRect(0, 0, ImageViewWidth+38, height)];
    if (self) {
        

        
        
        _imageView = [[NSImageView alloc] initWithFrame:NSMakeRect(8, 0, ImageViewWidth, height)];
        [self addSubview:_imageView];
        
        CGRect sessionsFrame = CGRectMake( 26, 11, 40, 15);
        _sessionsLabel = [[NSTextField alloc] initWithFrame: sessionsFrame];
        [_sessionsLabel setStringValue:@"---"];
        [_sessionsLabel setBezeled:NO];
        [_sessionsLabel setDrawsBackground:NO];
        [_sessionsLabel setEditable:NO];
        [_sessionsLabel setSelectable:NO];
        [[_sessionsLabel cell] setBackgroundStyle:NSBackgroundStyleRaised];
        [_sessionsLabel setFont:[NSFont fontWithName:@"AvenirNext-DemiBold" size:10.0]];
        [self addSubview:_sessionsLabel];
        
        //        CGRect labelFrame2 = CGRectMake( 26, 3, 40, 14);
        //        _onlineUsersLabel = [[NSTextField alloc] initWithFrame: labelFrame2];
        //        [_onlineUsersLabel setStringValue:@"---"];
        //        [_onlineUsersLabel setBezeled:NO];
        //        [_onlineUsersLabel setDrawsBackground:NO];
        //        [_onlineUsersLabel setEditable:NO];
        //        [_onlineUsersLabel setSelectable:NO];
        //        [_onlineUsersLabel setTextColor:[NSColor blueColor]];
        //        [[_onlineUsersLabel cell] setBackgroundStyle:NSBackgroundStyleRaised];
        //        [_onlineUsersLabel setFont:[NSFont fontWithName:@"AvenirNext-DemiBold" size:11.0]];
        //        [self addSubview:_onlineUsersLabel];
        
        CGRect labelFrame3 = CGRectMake( 30, 3, 40, 14);
        _IAPsLabel = [[NSTextField alloc] initWithFrame: labelFrame3];
        [_IAPsLabel setStringValue:@"--"];
        [_IAPsLabel setBezeled:NO];
        [_IAPsLabel setDrawsBackground:NO];
        [_IAPsLabel setEditable:NO];
        [_IAPsLabel setSelectable:NO];
        [_IAPsLabel setTextColor:[NSColor colorWithRed:.3 green:.6 blue:.1 alpha:1.0]];
        [[_IAPsLabel cell] setBackgroundStyle:NSBackgroundStyleRaised];
        [_IAPsLabel setFont:[NSFont fontWithName:@"AvenirNext-DemiBold" size:11.0]];
        [self addSubview:_IAPsLabel];
        
        

    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


- (void)mouseDown:(NSEvent *)theEvent {
    NSLog(@"%s",__func__);
    [_delegate togglePopover];
}

@end

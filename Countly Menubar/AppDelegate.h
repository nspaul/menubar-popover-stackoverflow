//
//  MFAppDelegate.h
//  StatusItemPopup
//
//  Created by Maxim Pervushin on 11/28/12.
//  Copyright (c) 2012 Maxim Pervushin. All rights reserved.
//

// https://github.com/maxim-pervushin/NSPopover-From-Status-Bar-Item

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end

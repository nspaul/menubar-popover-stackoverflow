//
//  CountlySettings.m
//  Countly Menubar
//
//  Created by Paul Brown on 2014/11/23.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import "CountlySettings.h"

@implementation CountlySettings

-(instancetype)init {
    
    NSDictionary *mainDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CountlyParameters" ofType:@"plist"]];

    
    NSString *appID = [mainDictionary objectForKey:@"appID"];
    NSString *apiKey = [mainDictionary objectForKey:@"apiKey"];
    NSString *sessionsURLString = [NSString stringWithFormat:@"http://countly.objectivejellyfish.com/o?app_id=%@&api_key=%@&action=refresh&method=sessions",appID,apiKey];
    self.sessionsURL = [NSURL URLWithString:sessionsURLString];
    
    return self;
}

@end

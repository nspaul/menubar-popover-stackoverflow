//
//  CountlySettings.h
//  Countly Menubar
//
//  Created by Paul Brown on 2014/11/23.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountlySettings : NSObject

@property (nonatomic) NSURL *sessionsURL;


@end

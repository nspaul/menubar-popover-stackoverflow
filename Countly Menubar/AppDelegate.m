#import "AppDelegate.h"
#import "StatusViewController.h"
#import "PopoverContentViewController.h"
#import "Fetcher.h"

@interface AppDelegate ()
{
    StatusViewController *_statusViewController;
    Fetcher *fetcher;
}



@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _statusViewController = [[StatusViewController alloc] init];
    
    fetcher = [[Fetcher alloc] init];
    fetcher.numberOfSessions = 0;
    
    [fetcher addObserver:_statusViewController forKeyPath:@"numberOfSessions" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    [fetcher addObserver:_statusViewController forKeyPath:@"numberOfIAPs" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

@end

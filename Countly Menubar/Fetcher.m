//
//  Fetcher.m
//  Countly Menubar
//
//  Created by Paul Brown on 2014/08/27.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import "Fetcher.h"
#import "CountlySettings.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define kcountlyIAPURL [NSURL URLWithString:@"http://countly.objectivejellyfish.com/o?app_id=545d90d2682623fa2f000002&api_key=35fe7507b00a04796a2f5c4aa30a09a0&method=events&event=transactionComplete&action=refresh"]


@interface Fetcher ()
{
    NSDateFormatter *dateFormatter;
    NSDate *now;
    NSString *dayString;
    NSString *monthString;
    NSString *yearString;
    NSString *tempDayString;
    
    CountlySettings *countlySettings;
}



@end

@implementation Fetcher

- (id)init {
    self = [super init];
    if (self) {
        
        countlySettings = [[CountlySettings alloc] init];
        
        self.numberOfIAPs = [NSNumber numberWithInt:0];
        self.numberOfSessions = [NSNumber numberWithInt:0];
        
        now = [NSDate date];
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d"];
        dayString = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"M"];
        monthString = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"y"];
        yearString = [dateFormatter stringFromDate:now];
        
        tempDayString = [NSString stringWithString:dayString];
        
        
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(fetchNumberOfSessions) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(fetchNumberOfSessions) userInfo:nil repeats:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(fetchNumberOfIAPs) userInfo:nil repeats:NO];
        [NSTimer scheduledTimerWithTimeInterval:300 target:self selector:@selector(fetchNumberOfIAPs) userInfo:nil repeats:YES];
        
        [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkDate) userInfo:nil repeats:YES];

        
    }
    return self;
}

-(void)checkDate {
    now = [NSDate date];
    [dateFormatter setDateFormat:@"d"];
    tempDayString = [dateFormatter stringFromDate:now];
    NSLog(@"checkDate");
    NSLog(@"tempDayString: %@", tempDayString);
    NSLog(@"dayString: %@", dayString);
    
    if (![tempDayString isEqualToString:dayString]) {
        NSLog(@"![tempDayString isEqualToString:dayString]");
        now = [NSDate date];
        
        self.numberOfIAPs = [NSNumber numberWithInt:0];
        
        [dateFormatter setDateFormat:@"d"];
        dayString = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"M"];
        monthString = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"y"];
        yearString = [dateFormatter stringFromDate:now];
    }
}



-(void)fetchNumberOfSessions {
    NSLog(@"%s",__func__);
    
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:countlySettings.sessionsURL];
        [self performSelectorOnMainThread:@selector(fetchedDataSessions:)
                               withObject:data
                            waitUntilDone:YES];
    });
}

-(void)fetchNumberOfIAPs {
    //    NSLog(@"%s",__func__);
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:kcountlyIAPURL];
        [self performSelectorOnMainThread:@selector(fetchedDataIAP:)
                               withObject:data
                            waitUntilDone:YES];
    });
}


- (void)fetchedDataSessions:(NSData *)responseData {
//    NSLog(@"%s",__func__);
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                         options:kNilOptions
                                                           error:&error];
    NSString* allSessionsToday = [[[[json objectForKey:yearString] valueForKey:monthString] valueForKey:dayString] valueForKey:@"t"];
    
    NSNumber *numberOfSessionsFromArray = [NSNumber numberWithInteger:[allSessionsToday integerValue]];
    NSNumber *currentNumberOfSessions = self.numberOfSessions;
    
    if (![currentNumberOfSessions isEqualToNumber:numberOfSessionsFromArray]) {
        self.numberOfSessions = numberOfSessionsFromArray;
    }
    

    
}




- (void)fetchedDataIAP:(NSData *)responseData {
    NSLog(@"%s",__func__);
    NSError* error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData
                                                         options:kNilOptions
                                                           error:&error];
    
    NSString *allIAPsToday = [[[[jsonArray objectAtIndex:0] valueForKey:yearString] valueForKey:monthString] valueForKey:dayString];
    NSNumber *numberOfIAPsFromArray = [NSNumber numberWithInt:0];
    if (allIAPsToday) {
        numberOfIAPsFromArray = [allIAPsToday valueForKey:@"c"];
    }
    
    NSNumber *currentNumberOfIAPs = self.numberOfIAPs;
    
    if (![numberOfIAPsFromArray isEqualToNumber:currentNumberOfIAPs]) {
        self.numberOfIAPs = numberOfIAPsFromArray;
        [self sendNotificationToUser];
    }
}


-(void)sendNotificationToUser {
    NSLog(@"%s",__func__);
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = @"In-App Purchase!";
    notification.informativeText = [NSString stringWithFormat:@"Someone Paid Up!"];
    notification.soundName = NSUserNotificationDefaultSoundName;
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];

}


// Realtime users. For Enterprise Countly

//-(void)fetchNumberOfOnlineUsers {
//    dispatch_async(kBgQueue, ^{
//        NSData *data = [NSData dataWithContentsOfURL:kcountlyOnlineUsersURL];
//        [self performSelectorOnMainThread:@selector(fetchedDataOnlineUsers:)
//                               withObject:data
//                            waitUntilDone:YES];
//    });
//}

//- (void)fetchedDataOnlineUsers:(NSData *)responseData {
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
//                                                         options:kNilOptions
//                                                           error:&error];
//    self.numberOfOnlineUsers = [json objectForKey:@"o"];
//}



@end

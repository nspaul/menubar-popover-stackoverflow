//
//  MFStatusView.h
//  ComplexStatusItem
//
//  Created by Maxim Pervushin on 11/27/12.
//  Copyright (c) 2012 Maxim Pervushin. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface StatusViewController : NSViewController <NSMenuDelegate, NSPopoverDelegate>
{

}

@property (nonatomic) NSTextField *sessionsLabel;
@property (nonatomic) NSTextField *onlineUsersLabel;
@property (nonatomic) NSTextField *IAPsLabel;

- (void)showPopoverWithViewController:(NSViewController *)viewController;
- (void)hidePopover;
-(void)togglePopover;

@end

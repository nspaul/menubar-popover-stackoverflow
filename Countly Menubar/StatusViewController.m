//
//  MFStatusView.m
//  ComplexStatusItem
//
//  Created by Maxim Pervushin on 11/27/12.
//  Copyright (c) 2012 Maxim Pervushin. All rights reserved.
//

#import "StatusViewController.h"
#import "PopoverContentViewController.h"
#import "Popover.h"
#import "StatusView.h"

#define ImageViewWidth 22

@interface StatusViewController ()
{
    BOOL _statusViewIsActive;
    
    NSImageView *_imageView;
    
    NSStatusItem *_statusItem;
    NSMenu *_statusItemMenu;
    
    NSPopover *_popover;
    PopoverContentViewController *_mfp;
    
    StatusView *statusView;
    
    NSEvent *popoverTransiencyMonitor;
}

@property (nonatomic) NSEvent *popoverTransiencyMonitor;

- (void)updateUI;
- (void)setActive:(BOOL)active;

- (void)quitMenuItemAction:(id)sender;

@end

@implementation StatusViewController

- (id)init
{

    _mfp = [[PopoverContentViewController alloc] initWithNibName:@"PopoverContentViewController" bundle:nil];
    
    
    self = [super init];
    if (self) {
        
        _statusViewIsActive = NO;
        statusView = [[StatusView alloc] init];
        statusView.delegate = self;
        
        _statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
        _statusItem.view = statusView;
        
        _statusItemMenu = [[NSMenu alloc] init];


        [self updateUI];
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
//    NSLog(@"%s",__func__);
    if (_statusViewIsActive) {
        [[NSColor selectedMenuItemColor] setFill];
        NSRectFill(dirtyRect);
    } else {
        [[NSColor clearColor] setFill];
        NSRectFill(dirtyRect);
    }
}




-(void)togglePopover {
    NSLog(@"%s",__func__);
//    [self setStatusViewIsActive:!_statusViewIsActive];
    
    if (_statusViewIsActive) {
        [self hidePopover];
    } else {
        [self showPopoverWithViewController:_mfp];
    }
    
}


- (void)updateUI
{
    statusView.imageView.image = [NSImage imageNamed:_statusViewIsActive ? @"icon" : @"icon"];
}

- (void)setStatusViewIsActive:(BOOL)active
{
    _statusViewIsActive = active;
    [self updateUI];
    
}



- (void)showPopoverWithViewController:(NSViewController *)viewController
{
    NSLog(@"%s",__func__);
    if (_popover == nil) {
        _popover = [[NSPopover alloc] init];
        _popover.animates = NO;
        _popover.delegate = self;
        _popover.behavior = NSPopoverBehaviorSemitransient;
        _popover.contentViewController = viewController;
    }
    if (!_popover.isShown) {
        _popover.contentViewController = viewController;
        [_popover showRelativeToRect:statusView.frame
                              ofView:statusView
                       preferredEdge:NSMinYEdge];
        
        if (self.popoverTransiencyMonitor == nil) {
            self.popoverTransiencyMonitor = [NSEvent addGlobalMonitorForEventsMatchingMask:(NSLeftMouseDownMask | NSRightMouseDownMask | NSKeyUpMask) handler:^(NSEvent* event) {
                [NSEvent removeMonitor:self.popoverTransiencyMonitor];
                self.popoverTransiencyMonitor = nil;
//                [self setStatusViewIsActive:!_statusViewIsActive];
                [_popover close];
            }];
        }
    }
}

- (void)popoverWillShow:(NSNotification *)notification {
    NSLog(@"%s",__func__);
}

-(void)popoverDidShow:(NSNotification *)notification {
    [self setStatusViewIsActive:!_statusViewIsActive];
}


- (void)popoverWillClose:(NSNotification *)notification {
    NSLog(@"%s",__func__);
}

- (void)popoverDidClose:(NSNotification *)notification {
    [self setStatusViewIsActive:!_statusViewIsActive];
    _popover.contentViewController = nil;
}


- (void)hidePopover
{
    if (_popover != nil && _popover.isShown) {
        [_popover close];
    }
}

#pragma mark - NSMenuDelegate

- (void)menuDidClose:(NSMenu *)menu
{
    NSLog(@"%s",__func__);
    [self setActive:NO];
}


- (void)quitMenuItemAction:(id)sender
{
    [[NSApplication sharedApplication] terminate:self];
}



#pragma mark KVO Observing


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"numberOfSessions"])
    {
        NSNumber *oldC = [change objectForKey:NSKeyValueChangeOldKey];
        NSNumber *newC = [change objectForKey:NSKeyValueChangeNewKey];
        NSLog(@"oldC: %@, newC: %@",oldC, newC);
        if( (oldC < newC) || ([[NSNull null] isEqual:oldC]) ) {
            statusView.sessionsLabel.stringValue = [NSString stringWithFormat:@"%@",newC];
        }
        else {
        }
    }
    else if([keyPath isEqualToString:@"numberOfIAPs"])
    {
        NSLog(@"numberOfIAPs change");
        NSNumber *newC = [change objectForKey:NSKeyValueChangeNewKey];
        
        NSLog(@"newC: %@",newC);
        
        if (newC == 0) {
            statusView.IAPsLabel.stringValue = @"0";
        }
        else {
            statusView.IAPsLabel.stringValue = [NSString stringWithFormat:@"%@",newC];
        }
    }
    
}


@end

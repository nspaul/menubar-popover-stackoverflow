//
//  StatusView.h
//  Countly Menubar
//
//  Created by Paul Brown on 2014/11/24.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "StatusViewController.h"

@interface StatusView : NSView
{
    NSImageView *imageView;
    StatusViewController *_delegate;
}


@property (nonatomic) StatusViewController *delegate;
@property (nonatomic) NSImageView *imageView;

@property (nonatomic) NSTextField *sessionsLabel;
@property (nonatomic) NSTextField *onlineUsersLabel;
@property (nonatomic) NSTextField *IAPsLabel;


@end

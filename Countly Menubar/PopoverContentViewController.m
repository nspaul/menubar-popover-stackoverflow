//
//  MFPopoverContentViewController.m
//  StatusItemPopup
//
//  Created by Maxim Pervushin on 11/28/12.
//  Copyright (c) 2012 Maxim Pervushin. All rights reserved.
//

#import "PopoverContentViewController.h"
#import "PreferencesWindowController.h"
#import "StatusViewController.h"


@interface PopoverContentViewController () {
    IBOutlet NSButton *settingsButton;
}

@end

@implementation PopoverContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
        
        
        
    }
    
    return self;
}

- (IBAction)showPreferencesWindow:(id)sender
{
    NSLog(@"%s",__func__);
    PreferencesWindowController *sharedController = [PreferencesWindowController sharedPreferencesWindowController];
    [sharedController showWindow:sender];
}

@end

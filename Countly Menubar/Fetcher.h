//
//  Fetcher.h
//  Countly Menubar
//
//  Created by Paul Brown on 2014/08/27.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fetcher : NSObject


@property (nonatomic) NSNumber *numberOfAds;
@property (nonatomic) NSNumber *numberOfSessions;
@property (nonatomic) NSNumber *numberOfIAPs;
@property (nonatomic) NSNumber *numberOfOnlineUsers;





@end

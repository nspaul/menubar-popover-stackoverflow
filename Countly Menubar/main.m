//
//  main.m
//  Countly Menubar
//
//  Created by Paul Brown on 2014/08/18.
//  Copyright (c) 2014 Paul Brown. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
